cd ..
cd ..
rmdir /s /q publish
dotnet publish -c Release -o publish

cd publish
docker build -t devgitlab.cqiserv.com:5050/serv/drawplaystation:snake .
docker push devgitlab.cqiserv.com:5050/serv/drawplaystation:snake

cd ..
cd K8s/Snake
kubectl delete -f 4-Deploy
kubectl create -f 4-Deploy

pause