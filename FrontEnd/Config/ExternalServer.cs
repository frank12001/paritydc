﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd
{
    public class ExternalServers
    {
        public const string Section = "ExternalServers";

        public string WebResolver { get; set; }
    }
}
