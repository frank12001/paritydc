﻿using System;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace ConsoleApp2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //using (IWebDriver driver = new ChromeDriver())
            //{
            //    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            //    driver.Navigate().GoToUrl("https://www.bitopro.com/");
            //    Console.WriteLine("Title: "+ driver.Title);
            //    driver.FindElement(By.Name("q")).SendKeys("cheese" + Keys.Enter);
            //    wait.Until(webDriver => webDriver.FindElement(By.CssSelector("h3")).Displayed);
            //    IWebElement firstResult = driver.FindElement(By.CssSelector("h3"));
            //    Console.WriteLine(firstResult.GetAttribute("textContent"));
            //}

            var chromeOptions = new ChromeOptions();
            IWebDriver driver = new RemoteWebDriver(new Uri("http://127.0.0.1:4444/wd/hub"), chromeOptions);
            driver.Navigate().GoToUrl("https://www.huobi.com/zh-cn/");
            Console.WriteLine("Title: " + driver.Title);
            await Task.Delay(10000);
            string s = driver.PageSource;            
         

            Console.ReadKey();

            driver.Quit();
            driver.Close();
            driver.Dispose();
        }
    }
}
