﻿using HtmlAgilityPack;
using AngleSharp;
using System;
using System.Linq;
using CefSharp.OffScreen;
using CefSharp;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp.Html.Parser;

namespace ParityDC
{
    class Program
    {
        private static ChromiumWebBrowser browser;

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //// From Web
            //var url = "https://www.bitopro.com/";
            //var web = new HtmlWeb();
            //HtmlDocument doc = web.Load(url);

            //var config = Configuration.Default.WithDefaultLoader();
            //var address = "https://en.wikipedia.org/wiki/List_of_The_Big_Bang_Theory_episodes";
            //var context = BrowsingContext.New(config);
            //var document = await context.OpenAsync(address);
            //var cellSelector = "tr.vevent td:nth-child(3)";
            //var cells = document.QuerySelectorAll(cellSelector);
            //var titles = cells.Select(m => m.TextContent);

            ////We require a custom configuration
            //var config = Configuration.Default.WithJs().WithCss();

            ////Create a new context for evaluating webpages with the given config
            //var context = BrowsingContext.New(config);

            //    //This is our sample source, we will set the title and write on the document
            //    var source = @"<!doctype html>
            //<html>
            //<head><title>Sample</title></head>
            //<body>
            //<script>
            //document.title = 'Simple manipulation...';
            //document.write('<span class=greeting>Hello World!</span>');
            //</script>
            //</body>";            


            //var source = doc.Text;

            //var document = await context.OpenAsync(req => req.Content(source));

            //var p = document.CreateElement("script");
            //p.SetAttribute("src", @"https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js");
            //p.SetAttribute("type", "text/javascript");
            //document.Head.AppendChild(p);

            ////Modified HTML will be output
            //Console.WriteLine(document.DocumentElement.OuterHtml);
            //string s = document.DocumentElement.OuterHtml;

#if ANYCPU
            //Only required for PlatformTarget of AnyCPU
            AppDomain.CurrentDomain.AssemblyResolve += Resolver;
#endif

            const string testUrl = "https://pro.coinbase.com/trade/BTC-USD";

            Console.WriteLine("This example application will load {0}, take a screenshot, and save it to your desktop.", testUrl);
            Console.WriteLine("You may see Chromium debugging output, please wait...");
            Console.WriteLine();

            var settings = new CefSettings()
            {
                //By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
                //CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings);

            // Create the offscreen Chromium browser.
            browser = new ChromiumWebBrowser(testUrl);

            // An event that is fired when the first page is finished loading.
            // This returns to us from another thread.
            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            // We have to wait for something, otherwise the process will exit too soon.
            Console.ReadKey();

            string s = await browser.GetTextAsync();
            //while (true)
            //{ }

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();
        }

        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            // Check to see if loading is complete - this event is called twice, one when loading starts
            // second time when it's finished
            // (rather than an iframe within the main frame).
            if (!e.IsLoading)
            {
                // Remove the load event handler, because we only want one snapshot of the initial page.
                //browser.LoadingStateChanged -= BrowserLoadingStateChanged;

                var s = browser.GetTextAsync();
                s.ContinueWith(x =>
                {
                    Console.WriteLine(x.Result);
                }, TaskScheduler.Default);
            }

            // Will attempt to load missing assembly from either x86 or x64 subdir
            //when PlatformTarget is AnyCPU
#if ANYCPU
        private static System.Reflection.Assembly Resolver(object sender, ResolveEventArgs args)
        {
            if (args.Name.StartsWith("CefSharp.Core.Runtime"))
            {
                string assemblyName = args.Name.Split(new[] { ',' }, 2)[0] + ".dll";
                string archSpecificPath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                                                       Environment.Is64BitProcess ? "x64" : "x86",
                                                       assemblyName);

                return File.Exists(archSpecificPath)
                           ? System.Reflection.Assembly.LoadFile(archSpecificPath)
                           : null;
            }

            return null;
        }
#endif
        }
    }
}
