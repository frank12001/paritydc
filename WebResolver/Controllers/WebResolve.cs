﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CefSharp;
using CefSharp.OffScreen;

namespace WebResolver.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WebResolve : ControllerBase
    {
        private readonly ILogger<WebResolve> _logger;

        public WebResolve(ILogger<WebResolve> logger)
        {
            _logger = logger;
        }

        [HttpGet()]
        public async Task<string> Get(string url, int waitSecond)
        {
            // Create the offscreen Chromium browser.
            ChromiumWebBrowser browser = new ChromiumWebBrowser(url);

            bool end = false;

            browser.LoadingStateChanged += (sender, e) => {
                if (!e.IsLoading)
                { end = true; }
            };

            while (!end)
            {
                await Task.Delay(100);
            }

            await Task.Delay(TimeSpan.FromSeconds(waitSecond));
            string result = await browser.GetTextAsync();

            browser.Stop();
            browser.Delete();
            browser.Dispose();

            return result;
        }
    }
}
