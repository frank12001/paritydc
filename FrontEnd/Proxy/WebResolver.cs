﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.Extensions.Hosting;
using RestSharp;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace FrontEnd
{
    public class WebResolver : BackgroundService
    {
        private readonly RestRequest ApiWebResolver = new RestRequest("WebResolve", Method.GET);
        private readonly RestClient Client;
        private readonly ExternalServers Config;
        private readonly ILogger<WebResolver> Logger;

        private Uri BitoUrl = new Uri("https://www.bitopro.com/");

        public event Action<DCoin> EBitopro;
        public WebResolver(ILogger<WebResolver> logger, IOptions<ExternalServers> config)
        {
            Config = config.Value;
            Client = new RestClient(new Uri(Config.WebResolver));
            Logger = logger;
        }

        public async Task<string> Resolve(Uri url,TimeSpan waitingSec)
        {
            var request = new RestRequest(ApiWebResolver.Resource, ApiWebResolver.Method).AddQueryParameter("url", url.OriginalString).AddQueryParameter("waitSecond", ((int)waitingSec.TotalSeconds).ToString());
            var response = await Client.ExecuteAsync(request);
            return response.Content;
        }

        public string Bito()
        {
            return "";
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                string bitoText = await Resolve(BitoUrl, TimeSpan.FromSeconds(0));
                //Convert bitoText to DCoin
                DCoin BitoPro = bito(bitoText);

                
                Logger.LogInformation("DCoin From Bito (ETH,BTC): "+ BitoPro.ETH+","+BitoPro.BTC );

                EBitopro?.Invoke(BitoPro);

                await Task.Delay(TimeSpan.FromMilliseconds(10), stoppingToken);
            }

            static DCoin bito(string text)
            {
                DCoin result = new DCoin() 
                {
                    ETH = decimal.Parse(Regex.Replace(text.Substring("ETH/TWD", "TWD"), "[^.0-9]", "")),
                    BTC = decimal.Parse(Regex.Replace(text.Substring("BTC/TWD", "TWD"), "[^.0-9]", "")),
                };
                return result;
            }
        }
    }
}
