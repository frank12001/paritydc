using System;
using System.Linq;
using CefSharp.OffScreen;
using CefSharp;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace WebApplication2
{
    class Program
    {
        private static ChromiumWebBrowser browser;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Only required for PlatformTarget of AnyCPU
            AppDomain.CurrentDomain.AssemblyResolve += Resolver;

            const string testUrl = "https://www.bitopro.com/";

            Console.WriteLine("This example application will load {0}, take a screenshot, and save it to your desktop.", testUrl);
            Console.WriteLine("You may see Chromium debugging output, please wait...");
            Console.WriteLine();

            var settings = new CefSettings()
            {
                //By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
                //CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings);

            // Create the offscreen Chromium browser.
            browser = new ChromiumWebBrowser(testUrl);

            // An event that is fired when the first page is finished loading.
            // This returns to us from another thread.
            browser.LoadingStateChanged += BrowserLoadingStateChanged;

            // We have to wait for something, otherwise the process will exit too soon.
            //Console.ReadKey();
            while (true)
            { }

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();
        }

        private static void BrowserLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            // Check to see if loading is complete - this event is called twice, one when loading starts
            // second time when it's finished
            // (rather than an iframe within the main frame).
            if (!e.IsLoading)
            {
                // Remove the load event handler, because we only want one snapshot of the initial page.
                browser.LoadingStateChanged -= BrowserLoadingStateChanged;

                var s = browser.GetTextAsync();
                s.ContinueWith(x =>
                {
                    Console.WriteLine(x.Result);
                }, TaskScheduler.Default);
            }
        }

        // Will attempt to load missing assembly from either x86 or x64 subdir
        //when PlatformTarget is AnyCPU
        private static System.Reflection.Assembly Resolver(object sender, ResolveEventArgs args)
        {
            //if (args.Name.StartsWith("CefSharp.Core.Runtime"))
            //{
                string assemblyName = args.Name.Split(new[] { ',' }, 2)[0] + ".dll";
                string archSpecificPath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                                                       Environment.Is64BitProcess ? "x64" : "x86",
                                                       assemblyName);
            Console.Write($"archSpecificPath: {archSpecificPath}");
                return File.Exists(archSpecificPath)
                           ? System.Reflection.Assembly.LoadFile(archSpecificPath)
                           : null;
            //}

            return null;
        }
    }
}
