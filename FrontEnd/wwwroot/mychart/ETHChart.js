﻿"use strict";
class ETHChart {
    constructor() {
        this.LineChart = null;
    }
    Line(data1,min,max) {
        if (data1 === null)
        {
            this.LineChart.destroy
            this.LineChart = null;
            return;
        }
        if (this.LineChart === null) {

            const labels = ["Bito", "Binance", "Coinbase", "Huobi"];
            const data = {
                labels: labels,
                datasets: [
                    {
                        label: 'Price',
                        data: data1,
                        borderColor: '#4dc9f6',
                        backgroundColor: '#4dc9f6',
                    }
                ]
            };

            const config = {
                type: 'line',
                data: data,
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Min and Max Settings'
                        }
                    },
                    scales: {
                        y: {
                            min: min,
                            max: max,
                        }
                    }
                },
            };

            this.LineChart = new Chart(
                document.getElementById('ETHLine'),
                config
            );
        }
        else
        {
            this.LineChart.data.datasets[0].data = data1;
            this.LineChart.options = {
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: 'Min and Max Settings'
                    }
                },
                scales: {
                    y: {
                        min: min,
                        max: max,
                    }
                }
            };
            this.LineChart.update();
        }
    }
}

window[ETHChart.name] = new ETHChart();
