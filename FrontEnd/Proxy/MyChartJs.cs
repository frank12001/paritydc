﻿
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace FrontEnd
{
    public class MyChartJs
    {
        private readonly IJSRuntime JS;
        public MyChartJs(IJSRuntime jSRuntime)
        {
            JS = jSRuntime;
        }

        public async Task ETHLine(decimal[] data1, decimal min, decimal max)
        {
            await JS.InvokeVoidAsync("ETHChart.Line",
                data1,min,max);
        }
    }
}
